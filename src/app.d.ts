// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
interface User {
	id: string,
	name: string,
	token: string,
	role: string
}

interface SessionData {
	views: number,
	data1: number[],
	data2: string[],
	user: User
}
interface Todoload {
	views: number,
	data1: number[],
	data2: string[],
	user: User
}
interface Country {
	id: number,
	name: string
}

declare global {
	namespace App {
		interface Locals {
			session: import('svelte-kit-cookie-session').Session<SessionData>,

		}

		interface Session extends SessionData { }
		// interface Error {}

		interface Locals {
			stop: boolean
		}

		interface PageData { session: SessionData; }
		// interface Platform {}
	}
}

export { };
