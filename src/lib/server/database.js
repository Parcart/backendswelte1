// In a real app, this data would live in a database,

import { each } from "svelte/internal";

// rather than in memory. But for now, we cheat.
const db = new Map();

export function getTodos(userid) {
	return db.get(userid);
}


export function createTodo(userid, description, date) {
	if (description === '') {
		throw new Error('todo не должно быть пустое');
	}
	if (!db.has(userid)) {
		db.set(userid, []); 
	}
	const todos = db.get(userid);
	if (todos.find((todo) => todo.description === description)) {
		throw new Error('todos Не уникально');
	}
	
	todos.push({
		id: crypto.randomUUID(),
		description, date,
		done: false
	});
}

export function deleteTodo(userid, todoid) {
	const todos = db.get(userid);
	const index = todos.findIndex((todo) => todo.id === todoid);

	if (index !== -1) {
		todos.splice(index, 1);
	}
}
