import bcrypt from 'bcrypt'
import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').Actions} */
export const actions = {
    register: async ({ locals, cookies, url, request }) => {

        //получим ID роли USER
        let { data, error } = await supabase
            .from('roles')
            .select('role_id')
            .eq('role_name', 'USER')

        console.log(error)  
        console.log(data)   
        let user_role_id = data[0].role_id
        console.log(user_role_id, error)

        //Получим данные формы
        const formdata = await request.formData()
        let username = formdata.get("username")
        let password = formdata.get('password')
        let checkPassword = false;
        const myRe = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[\^\w\d\s:])([^\s]){8,16}$/gm;
		const result = password?.toString().replace(myRe, "bruh");
		if(result=="bruh"){
			checkPassword = true;
		}
		else{
			checkPassword = false;
		}

        if (!checkPassword) {
            return fail(409, { password: true , user: false })
        }
        //Проверим, нет ли уже такого польз-ля
        let user = await supabase
            .from('users')
            .select('user_login')
            .eq('user_login', username)


        if (user.data.length) {//такой польз-ль уже есть
            return fail(409, { user: true, password: false })
        }

        //Создадим польз-ля с ролью USER
        console.log(username, password, user_role_id, await bcrypt.hash(password, 10))
        const new_user = await supabase
            .from('users')
            .insert(
                {
                    user_login: username,
                    user_password: await bcrypt.hash(password, 10),
                    user_AuthToken: crypto.randomUUID(),
                    role_id: user_role_id
                }
            )
            console.log(new_user)
        throw redirect(303, '/login')

    }

}
