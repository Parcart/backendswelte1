import { json, type RequestHandler, redirect, fail } from '@sveltejs/kit';

export const GET: RequestHandler = async ({ locals, url }) => {
    locals.session.destroy()
    throw redirect(303, `/`);
};
