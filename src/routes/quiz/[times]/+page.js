import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').PageServerLoad} */
export async function load({ params }) {
    const { data } = await supabase.from("countries")
        .select()
        .order('id', { ascending: true })


    return {
        // special: { a: 123, b: 456 },
        // matter: ["a", "b", "c"],
        countries: data ?? [],
    }
};

