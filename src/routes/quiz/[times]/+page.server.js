
import { fail, redirect } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').PageServerLoad} */
export async function load({ locals }) {
    console.log('locals', locals)
    if (locals.stop == true) {
        console.log('!!!!!!!!')
        throw redirect(303, '/login')
    }

};



/**@type {import('./$types').Actions} */
export const actions = {
    update_country: async ({ locals, cookies, url, request }) => {
        const data = await request.formData()

        let name = data.get("name")
        let id = data.get('id')

        const { error } = await supabase
            .from('countries')
            .update({ name: name })
            .eq('id', id)

        if (error) {
            return fail(parseInt(error.code), { error: error.message });
        }
    }
}

    // check: async ({ locals, cookies, url, request, params }) => {

    //     const { data1 = [] } = locals.session.data;
    //     // await locals.session.set({ views: 222, data1: [], data2: [] });

    //     const data = await request.formData()
    //     let answer = data.get("answer")

    //     //стандартная робота с cookis
    //     let cook_adder = cookies.get('adder')
    //     cookies.set('adder', (parseInt(answer) + parseInt(cook_adder)).toString(10))

    //     //работа с состоянием через url-параметры
    //     let summ = url.searchParams.get('/check')
    //     let res = parseInt(summ) + parseInt(answer);


    //     if (parseInt(answer) == 100) {
    //         const { data1 = [] } = locals.session.data;
    //         if (data1.length == 0) {
    //             await locals.session.set({ views: 222, data1: [-321], data2: [] });
    //         } else {
    //             await locals.session.update((data) => ({ data1: [...data.data1, 5, 6, 7] }))

    //             //BAD!! await locals.session.update(({ data1 }) => [...data1, 333, 444])
    //         }
    //     }


    //     try {
    //         if (parseInt(answer) == 200) {
    //             await locals.session.update((data) => ({
    //                 user: { name: "qq", email: "qq@ww.com" },
    //                 data2: [...data.data2, 'qq', 'ww', 'ee'],
    //                 data1: [...data.data1, 555, 666, 777],
    //             }));
    //         }
    //     } catch (err) {
    //         console.log(err)
    //     }
    //     await locals.session.refresh(10)

    //     return { summa: 1000, text: '<h1>ТЕКСТ</h1>' }
    // }
