import { USER } from "$env/static/private";
import { supabase } from "$lib/supabaseClient";
import { fail, redirect } from '@sveltejs/kit';


export async function load({ locals }) {
    if (locals.session.data.user) {
    let x = await supabase
        .from('todo')
        .select()
        .eq('user_id', locals.session.data.user.id)
    console.log(x)
    return { "todoload": x.data?.sort(function(a, b){
             
        return new Date(b.data) - new Date(a.data);
    })}
    }
    else{
        throw redirect(302,"/login")
    }
    
}

export const actions = {
    add: async ({ locals, cookies, url, request }) => {
        const formdata = await request.formData()
        let todo_text = formdata.get("todo_text")
        let todo_complite_datetime = formdata.get("todo_complite_datetime")
        const add = await supabase
            .from('todo')
            .insert(
                {
                    todo_text: todo_text,
                    user_id: locals.session.data.user.id,
                    data: new Date(),
                    todo_complite: Boolean(false),
                    todo_complite_datetime: todo_complite_datetime,
                    
                }
            )  
    },
    delete: async ({ locals, cookies, url, request }) => {
        const formdata = await request.formData()
        let todo_id = formdata.get("todo_id")
        const deletetodo = await supabase
            .from('todo')
            .delete()
            .eq("todo_id", todo_id)
    },
    update: async ({ locals, cookies, url, request }) => {
        const formdata = await request.formData()
        let todo_id = formdata.get("todo_id")
        let todo_text = formdata.get("todo_text")
        let todo_complite_datetime = formdata.get("todo_complite_datetime")
        const { error } = await supabase
            .from('todo')
            .update({ todo_text: todo_text,
                todo_complite_datetime: todo_complite_datetime })
            .eq('todo_id', todo_id)
    }
}