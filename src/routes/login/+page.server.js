import bcrypt from 'bcrypt'
import { redirect, fail } from '@sveltejs/kit';
import { supabase } from "$lib/supabaseClient";

/**@type {import('./$types').Actions} */
export const actions = {
    login: async ({ locals, cookies, url, request }) => {
        //Получим данные формы
        const formdata = await request.formData()
        let username = formdata.get("username")
        let password = formdata.get('password')

        //Проверим, есть ли польз-ль
        let user = await supabase
            .from('users')
            .select()
            .eq('user_login', username)
        
        
        if (!user.data.length) {//Такого польз-ля нет
            return fail(400, { is_user: false })
        }

        //проверим пароль
        const checkPassword = await bcrypt.compare(password, user.data[0].user_password)
        console.log(checkPassword)
        if (!checkPassword) {//пароль неправильный
            return fail(400, { bad_pwd: true })
        }

        //обновим данные польз-ля
        let token = crypto.randomUUID()
        const upd_user = await supabase
            .from('users')
            .update({ user_AuthToken: token, updated_at: new Date() })
            .eq('user_id', user.data[0].user_id)
        console.log(upd_user)
        if (!upd_user.error) {//нет ошибок при update
            await locals.session.set({ user: { id: user.data[0].user_id, name: user.data[0].user_login, token: token, role: user.data[0].role_id}, views: 0, data1: [], data2: [] });

            throw redirect(303, '/')
        } else {//update вызвал ошибку

        }
    }
}
