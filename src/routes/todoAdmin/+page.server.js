
import { supabase } from "$lib/supabaseClient";
import { fail, redirect } from '@sveltejs/kit';
import { error } from '@sveltejs/kit';

export async function load({ locals }) {
	if (locals.session.data.user.role != 3) {
        throw error(404, {
            message: 'Not found, или ты ожидал чего то другого?)'
        });
    }
    else{
        let x = await supabase
        .from('todo')
        .select('todo_id, todo_text, user_id, data, todo_complite, todo_complite_datetime, users:user_id(user_id, user_login)')
        return { "todoload": x.data?.sort(function(a, b){
             
         return new Date(b.data) - new Date(a.data);
         })
        }
    }
    
}
